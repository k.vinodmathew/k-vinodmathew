Hello Guys,

I am highly ambitious to develop a simple soccer app using Xamarin Forms. This is targetted for a non-profit community soccer team. I would be very happy for those who are very keen 
to jump on and contribute. If its sound interesting feel free to clone the project and start building guys. Every bit of your code is worth, happy coding guys !!

For someone it might be bit nasty on the UI part, but don't worry ! eventually we will make it looking good and good for eyes :)

**Objective of the app:**

**What is done so far:**
  ```1.  Register a player.
     1.1 Input password and confirm password validation.
	 1.2 Photo capture in Register screen.
	 1.3 Email validation in Register screen.
	 1.4 Capture photo and save as byte into database.
	 1.5 Retrieving photo byte from database and display in Home page.
  2. Login/Logout.
  3. Display Players in Grid.
  4. Delete a player.
  6. Navigating to Home without login.
  8. Capture of soccer availability and update in Settings tab.
  9. Display of count in Daily count tab.
     9.1 Display of Image in circle shape in daily count tab.
  10.Update player details.
  11.IsBusy loading.
  14.Added screen shots of screen.
  15.Hide click here button under settings without login.
  16.Display random image in Map.
  18.Display of Image in Circle shape in Home page.
  19.Removed ProfilPic displayed during navigate flow.
  20.Aligned the records in Display Count page.
  21.Added an Updates page,Popup to capture title, news and save to database, then display back.
  22.Added text color delete button, image to remove the created update from the Updates tab.
  23.Code to 'update' the news/update details based on login player in the Update Page removed. Date will now display 'dddd, dd MMMM yyyy HH:mm:ss' in this format.
  24.Blank/Null,length validation done on title and content field is done.
  
```


**Not started yet:**

```4.1 Delete player should be possible only for Admin player.
   5. Display location of all players in Map and Account Settings.
   7. Forgot Password.
   12.Build a payment module using stripe api or similar.
   13.Notification of soccer status by players.
   17.Mask password field```
   
   
**Issues:**

```1.If the user perform photo capture and choose to close/ ignore it without selecting it, throws exception.
   2.Forgot password is not working.
   3.~~Need to remove the 'update' option from UpdatePopup screen--Fixed now~~
   4.On navigate the `Remove` button on Updates page should not display
   5.Remove button should fit into the UpdateNews.xaml layout properly```


  

