﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace soccerapp
{
    public class UpdateDetails : INotifyPropertyChanged
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        
        string fullname;
        string email;
        string title;
        string newscontent;
        string currentdate;

        public UpdateDetails(){

        }

        public string FullName
        {
            set
            {
                if (fullname != value)
                {
                    fullname = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("FullName"));
                    }
                }
            }
            get
            {
                return fullname;
            }
        }

        public string Email
        {
            set
            {
                if (email != value)
                {
                    email = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Email"));
                    }
                }
            }
            get
            {
                return email;
            }

        }

        public string Title
        {
            set
            {
                if (title != value)
                {
                    title = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Title"));
                    }
                }
            }

            get
            {
                return title;
            }


        }

        public string NewsContent
        {
            set
            {
                if (newscontent != value)
                {
                    newscontent = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("NewsContent"));
                    }
                }
            }

            get
            {
                return newscontent;
            }

        }

        public string CurrentDate
        {
            set
            {
                if (currentdate != value)
                {
                    currentdate = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("CurrentDate"));
                    }
                }
            }
            get
            {
                return currentdate;
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
    }
}
