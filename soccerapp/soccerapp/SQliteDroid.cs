﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using soccerapp.Droid;
using soccerapp.Droid.SQliteDroid;
using SQLite;

[assembly: Xamarin.Forms.Dependency(typeof(SQliteDroid))]
namespace soccerapp.Droid.SQliteDroid
{
    public class SQliteDroid : Isqlite
    {
        public SQLiteConnection GetConnection()
        {
            var dbase = "soccerpep";
            var dbpath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(dbpath, dbase);
            var connection = new SQLiteConnection(path);
            return connection;

        }
    }
}
