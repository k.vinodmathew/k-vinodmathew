﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace soccerapp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpdatePopup : Rg.Plugins.Popup.Pages.PopupPage
    {
        private SQLiteConnection conn;
        string emailTextFromUpdateNews;

        public UpdatePopup(string email)
        {
            InitializeComponent();
            //Rg.Plugins.Popup.Pages.PopupPage.AnimationProperty();
            String newsDate = DateTime.Now.ToString();
            conn = DependencyService.Get<Isqlite>().GetConnection();
            conn.CreateTable<UpdateDetails>();
            emailTextFromUpdateNews = email;


        }

        public async void OnSaveClicked(object sender, EventArgs e)
        {

            List<PlayerDetails> myList = (from x in conn.Table<PlayerDetails>().Where(x => x.Email == emailTextFromUpdateNews) select x).ToList();
            
            for (int i = 0; i < myList.Count; i++)
            {
                string fullName = myList[i].FullName;
                string email = myList[i].Email;

                string title = NewsTitle.Text;
                //string newscontent = NewsUpdates.Text;
                UpdateDetails updateDetails = new UpdateDetails();
                updateDetails.FullName += fullName;
                updateDetails.Email += email;
                updateDetails.Title += title;
                string newsupdatetext = NewsUpdates.Text;
                if(!string.IsNullOrWhiteSpace(NewsTitle.Text) && !string.IsNullOrWhiteSpace(NewsUpdates.Text))
                {
                    bool somevalue = LenghtOfString(newsupdatetext);
                    if (somevalue == true)
                    {
                        int expNumberOfLetters = 100;
                        int numberOfLetters = newsupdatetext.ToCharArray().Count();
                        int diffNumber = expNumberOfLetters - numberOfLetters;

                        await DisplayAlert("Text Limit", "Number of characters exceeded by" + " " + diffNumber, "OK");
                    }
                    else
                    {
                        updateDetails.NewsContent += newsupdatetext;
                        //updateDetails.NewsContent += newscontent;
                        updateDetails.CurrentDate = DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss");
                        int y = 0;
                        try
                        {
                            if (!string.IsNullOrWhiteSpace(NewsTitle.Text) && !string.IsNullOrWhiteSpace(NewsUpdates.Text))
                            {
                                //Insert the soccer status to the database:
                                y = conn.Insert(updateDetails);
                            }
                            else
                            {
                                await DisplayAlert("Updates", "News fields cannot be blank", "OK");
                            }

                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        if (y == 1)
                        {
                            await Navigation.PushAsync(new UpdatesNews(updateDetails, email));
                            await PopupNavigation.Instance.PopAsync();

                        }
                    }
                }
                else
                {
                     await DisplayAlert("Updates", "Title or News Content cannot be left blank", "OK");
                }

            }
 
        }

        public bool LenghtOfString(string text)
        {
            int numberOfLetters = text.ToCharArray().Count();
            if (numberOfLetters >100)
            {
                return true;
            }
            return false;
        }
    }
}