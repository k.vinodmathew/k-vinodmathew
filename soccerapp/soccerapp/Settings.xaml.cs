﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace soccerapp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Settings : ContentPage
    {
        private SQLiteConnection conn;
        SoccerAvailability status;
        string emailTextVal;

        public Settings(SoccerAvailability soccerStatus, string emailText, bool ShowButton = false)
        {
            InitializeComponent();            
            conn = DependencyService.Get<Isqlite>().GetConnection();
            conn.CreateTable<SoccerAvailability>();
            status = soccerStatus;
            emailTextVal = emailText;
            BindingContext = status;  
            button1.IsVisible = ShowButton;
            
        }

        public void OnButtonClicked(object sender, EventArgs e)
        {
            EnteredStatus.Text = string.Empty;
            overlay.IsVisible = true;
            EnteredStatus.Focus();
        }

        public void OnCancelButtonClicked(object sender, EventArgs e)
        {
            overlay.IsVisible = false;

        }

        public void OnOKButtonClicked(object sender, EventArgs e)
        {
            overlay.IsVisible = false;
            int count = (from x in conn.Table<PlayerDetails>().Where(x => x.Email == emailTextVal) select x).Count();
            if (count != 0)
            {
                var availability = (from x in conn.Table<SoccerAvailability>().Where(x => x.Email == emailTextVal) select x);
                foreach (var available in availability)
                {
                    string statusText = EnteredStatus.Text;
                    available.SoccerStatus += statusText.ToUpper().ToString();  
                    available.CurrentDate += DateTime.Now.ToString("ddMMyyyy");
                    // assuming conn is an SQLiteConnection
                    conn.Update(available);
                }
            }
            {
                List<PlayerDetails> details = conn.Table<PlayerDetails>().Where(x => x.Email == emailTextVal).ToList();
                SoccerAvailability soccerAvailability = new SoccerAvailability();
                soccerAvailability.PlayerImage = details[0].PlayerImage;
                soccerAvailability.FullName = details[0].FullName;
                soccerAvailability.Email = emailTextVal;
                string statusText = EnteredStatus.Text;
                soccerAvailability.SoccerStatus = statusText.ToUpper().ToString();
                soccerAvailability.CurrentDate = DateTime.Now.ToString("ddMMyyyy");
                var dailySoccerStatus = EnteredStatus.Text;
                int y = 0;
                try
                {
                    if (!string.IsNullOrWhiteSpace(EnteredStatus.Text))
                    {
                        //Insert the soccer status to the database:
                        y = conn.Insert(soccerAvailability);
                    }
                    else
                    {
                        DisplayAlert("Soccer Availability", "Availability cannot be left blank", "OK");
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                if (y == 1)
                {
                    //Navigation.PushAsync(new Settings(soccerAvailability, emailTextVal));
                    Navigation.PushAsync(new SoccerDailyStatus(soccerAvailability));
                }

            }
           
        }

    }
}