﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace soccerapp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Register : ContentPage
    {
        public SQLiteConnection conn;
        PlayerDetails myDetails;
        string playerEmail;
        Stream imageStream = new MemoryStream();


        public object SaveToAlbum { get; private set; }

        public Register(PlayerDetails playD)
        {
            InitializeComponent();
            conn = DependencyService.Get<Isqlite>().GetConnection();
            conn.CreateTable<PlayerDetails>();
            playerEmail = playD.Email;
            myDetails = playD;
            if (!string.IsNullOrEmpty(playD.FullName) || !string.IsNullOrEmpty(playD.Email))
                registerButton.Text = "Update";
                //emailRegister.IsReadOnly = true;
            BindingContext = myDetails;
            this.IsBusy = false;
            
        }

        public void TrySave_OnClicked(object sender, EventArgs e)
        {
            var imageArr = ReadFully(imageStream);
        }

        public async void RegisterSave_OnClicked(object sender, EventArgs e)
        {
            byte[] imageArr = await ReadFully(imageStream);
            this.IsBusy = true;
            await Task.Delay(TimeSpan.FromMilliseconds(1000));
            
            int count = (from y in conn.Table<PlayerDetails>().Where(y => y.Email == playerEmail) select y).Count();
            if(count!=0)
            {
                var updatePlayer = (from y in conn.Table<PlayerDetails>().Where(y => y.Email == playerEmail) select y);
                foreach (var update_Player in updatePlayer)
                {
                    update_Player.FullName = fullNameEntry.Text;
                    update_Player.Mobile = mobileEntry.Text;
                    update_Player.SoccerPosition = soccerpostionEntry.Text;
                    update_Player.PlayerImage = imageArr;
                    update_Player.Password = passwordEntry.Text;
                    update_Player.ConfirmPassword = confirmpasswordEntry.Text;
                    // assuming conn is an SQLiteConnection
                    conn.Update(update_Player);   
                }
                
                await Navigation.PushAsync(new MainPage());

            }
            else
            {
                PlayerDetails playerDetails = new PlayerDetails();
                playerDetails.FullName = fullNameEntry.Text;
                playerDetails.Mobile = mobileEntry.Text;
                playerDetails.SoccerPosition = soccerpostionEntry.Text;
                playerDetails.PlayerImage = imageArr;               
                playerDetails.Password = passwordEntry.Text;
                playerDetails.ConfirmPassword = confirmpasswordEntry.Text;
                int x = 0;
                try
                {
                    if (!string.IsNullOrWhiteSpace(fullNameEntry.Text) && !string.IsNullOrWhiteSpace(mobileEntry.Text) && !string.IsNullOrWhiteSpace(soccerpostionEntry.Text) && !string.IsNullOrWhiteSpace(emailRegister.Text) && !string.IsNullOrWhiteSpace(passwordEntry.Text))
                    {
                        bool email = ValidateEmail(emailRegister.Text);
                        if (email==true)
                        {
                            playerDetails.Email = emailRegister.Text;

                            var inputPass = passwordEntry.Text;
                            var confirmPass = confirmpasswordEntry.Text;
                            if (inputPass != confirmPass)
                            {
                                this.IsBusy = false;
                                await DisplayAlert("Register Alert", "Password and Confirm Password should be same", "OK");

                            }
                            else
                            {
                                //Insert the player to the database:
                                x = conn.Insert(playerDetails);
                            }

                        }
                        else
                        {
                            await DisplayAlert("Registration", "Please enter a valid email", "Cancel");
                        }
                               
                    }
                    else
                    {
                        this.IsBusy = false;
                        await DisplayAlert("Register Alert", "Fields cannot be left blank", "OK");
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    IsBusy = false;
                }

                if (x == 1)
                {
                    await DisplayAlert("Registration", "Player Registered Successfully", "Cancel");
                    await Navigation.PushAsync(new MainPage());
                }
                else
                {
                    this.IsBusy = false;
                } 

            }
           
        }

        private async void UploadButton_Clicked(object sender, EventArgs e)
        {
            //myDetails.Image = new Image();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
                return;
            }
                try
                {
                    var status = await GetPermissions();

                   if(status == true)
                    {
                    var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                    {
                        Directory = "Pictures",
                        //SaveToAlbum = true,
                        Name = "test.jpg",
                        PhotoSize = PhotoSize.Small,
                        CompressionQuality = 75,
                        CustomPhotoSize = 5,
                        //PhotoSize = PhotoSize.MaxWidthHeight,
                        DefaultCamera = CameraDevice.Front,
                        //Name = $"{DateTime.UtcNow}.jpg"
                    });
                    
                    imageStream = file.GetStream();
                    if (file == null)
                        return;

                    await DisplayAlert("File Location", (saveToGallery.IsToggled ? file.AlbumPath : file.Path), "OK");
                    
                    imageToUpload.Source = ImageSource.FromStream(() =>
                    {
                        var stream = file.GetStream();
                        file.Dispose();
                        return stream;
                    });
                }
                   else
                   {
                    await DisplayAlert("Permissions Denied", "Unable to take photos.", "OK");

                    //On iOS you may want to send your user to the settings screen.
                    if (Device.RuntimePlatform == Device.iOS)
                        CrossPermissions.Current.OpenAppSettings();

                } 
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Last, exception! " + ex);

                }
   
        }

        public static async Task<bool> GetPermissions()
        {
            bool permissionsGranted = true;

            var permissionsStartList = new List<Permission>()
        {
            Permission.Location,
            Permission.LocationAlways,
            Permission.LocationWhenInUse,
            Permission.Storage,
            Permission.Camera
        };

            var permissionsNeededList = new List<Permission>();
            try
            {
                foreach (var permission in permissionsStartList)
                {
                    var status = await CrossPermissions.Current.CheckPermissionStatusAsync(permission);
                    if (status != PermissionStatus.Granted)
                    {
                        permissionsNeededList.Add(permission);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Nice, exception! " + ex);
            }

            var results = await CrossPermissions.Current.RequestPermissionsAsync(permissionsNeededList.ToArray());

            try
            {
                foreach (var permission in permissionsNeededList)
                {
                    var status = PermissionStatus.Unknown;
                    //Best practice to always check that the key exists
                    if (results.ContainsKey(permission))
                        status = results[permission];
                    if (status == PermissionStatus.Granted || status == PermissionStatus.Unknown)
                    {
                        permissionsGranted = true;
                    }
                    else
                    {
                        permissionsGranted = false;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Last, exception! " + ex);
            }
            return permissionsGranted;
        }

        public static async Task<byte[]> ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int bytesRead;
                while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Position = 0;
                    Console.WriteLine("Our total=" + bytesRead);
                    ms.Write(buffer, 0, bytesRead);
                }
                byte[] result = ms.ToArray();
                return result;
            }
        }

        Regex EmailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        public bool ValidateEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            return EmailRegex.IsMatch(email);
        }

    }
}