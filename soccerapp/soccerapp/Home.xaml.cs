﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace soccerapp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Home : ContentPage
    {
        public SQLiteConnection conn = null;
        ObservableCollection<PlayerDetails> players;
        string loginEmail;
     

        public Home(string email)
        {
            InitializeComponent();
            conn = DependencyService.Get<Isqlite>().GetConnection();
            conn.CreateTable<PlayerDetails>();
            players = new ObservableCollection<PlayerDetails>();
            collectionview.ItemsSource = players;
            loginEmail = email;
            GetLoginPlayerName();
            DisplayDetails();
        }

        public void GetLoginPlayerName()
        {
            int count = (from x in conn.Table<PlayerDetails>().Where(x => x.Email == loginEmail) select x).Count();
            if (count!= 0)
            {
                
                var detail = conn.Table<PlayerDetails>().First(x => x.Email == loginEmail);
                loginPlayer.Text = detail.FullName;                   
            }
            
        }

        public void DisplayDetails()
        {
            List<PlayerDetails> details = (from x in conn.Table<PlayerDetails>() select x).ToList();
            
            for (int i = 0; i < details.Count; i++)
            {
                players.Add(details[i]);
                byte[] byteImage = details[i].PlayerImage;
                //ImageSource imageSource = ImageSource.FromStream(() => new MemoryStream(byteImage));
                //image.Source = imageSource;//binding in code
            }

        }

        public async void DeleteButton_OnClicked(object sender, System.EventArgs e)
        {
            bool res = await DisplayAlert("Message", "Do you want to delete a player ?", "Ok", "Cancel");
            if(res)
            {
                List<PlayerDetails> item = new List<PlayerDetails>();
                var playerDetails = (sender as Button).BindingContext as PlayerDetails;
                players.Remove(playerDetails);
                conn.Delete(playerDetails);
            }
        }

        private async void OnLogoutButtonClicked(object sender, EventArgs e)
        {
            MainPage.IsUserLoggedIn = false;
            var answer = await DisplayAlert("Logout", "Do you wish to Logout from the Soccer app", "OK", "Cancel");
            if (answer)
            {
                await Navigation.PushAsync(new MainPage());
            }
            

        }


    }
}