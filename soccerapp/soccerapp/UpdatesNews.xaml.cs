﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace soccerapp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [System.ComponentModel.DesignTimeVisible(false)]
    public partial class UpdatesNews : ContentPage
    {
        public SQLiteConnection conn = null;
        ObservableCollection<UpdateDetails> updateDetails;
        string emailTextvalue;
        public UpdatesNews(UpdateDetails details, string email, bool showPlusBtn = false, bool showRemoveBtn = false)
        {
            InitializeComponent();
            conn = DependencyService.Get<Isqlite>().GetConnection();
            conn.CreateTable<UpdateDetails>();
            updateDetails = new ObservableCollection<UpdateDetails>();
            LstTest.ItemsSource = updateDetails;
            emailTextvalue = email;
            plusButton.IsVisible = showPlusBtn;
            
            DisplayNews();
        }
        // ItemTapped="LstTest_ItemTapped"  ( add this xaml if needed)
        // private async void LstTest_ItemTapped(object sender, ItemTappedEventArgs e)
        // {
        // await DisplayAlert("Updates", "Display Some alert", "Cancel");
        //}

        public async void OnPlusButtonClicked(object sender, EventArgs e)
        {
            var page = new UpdatePopup(emailTextvalue);
            await PopupNavigation.Instance.PushAsync(page);
        }

        public void DisplayNews()
        {
            List<UpdateDetails> newsdetails = (from x in conn.Table<UpdateDetails>() orderby x.CurrentDate descending select x).ToList();

            for (int i = 0; i < newsdetails.Count; i++)
            {
                updateDetails.Add(newsdetails[i]);
            }
        }

        public async void OnRemoveButtonClicked(object sender, System.EventArgs e)
        {
            bool res = await DisplayAlert("Message", "Do you want to delete this update ?", "Ok", "Cancel");
            if (res)
            {
                List<UpdateDetails> item = new List<UpdateDetails>();
                var updatedetails = (sender as Button).BindingContext as UpdateDetails;
                updateDetails.Remove(updatedetails);
                conn.Delete(updatedetails);
            }
        }

       
    }
    
    
}