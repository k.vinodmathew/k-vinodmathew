﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace soccerapp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SoccerDailyStatus : ContentPage
    {
        private SQLiteConnection conn;
        SoccerAvailability status;
        SoccerAvailability count;


        ObservableCollection<SoccerAvailability> soccerAvailability;

        public int Count { get; private set; }

        public SoccerDailyStatus(SoccerAvailability soccerStatus)
        {
            InitializeComponent();
            conn = DependencyService.Get<Isqlite>().GetConnection();
            conn.CreateTable<SoccerAvailability>();
            status = soccerStatus;
            BindingContext = status;
            BindingContext = count;
            soccerAvailability = new ObservableCollection<SoccerAvailability>();
            listView.ItemsSource = soccerAvailability;           
            DisplaySoccerStatus();
            DisplayCount();

        }
 
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            DisplayCount();
        }

        public void DisplaySoccerStatus()
        {
            var datetoday = DateTime.Now.ToString("ddMMyyyy");
            List<SoccerAvailability> myList = (from x in conn.Table<SoccerAvailability>().Where(x => x.CurrentDate == datetoday) select x).ToList();

            for (int i = 0; i < myList.Count; i++)
            {
                soccerAvailability.Add(myList[i]);
                byte[] byteImage = myList[i].PlayerImage;
            }
        }

        public void DisplayCount()
        {
            var datetoday = DateTime.Now.ToString("ddMMyyyy");
            var count_in = (from x in conn.Table<SoccerAvailability>().Where(x => x.SoccerStatus == "IN" && x.CurrentDate== datetoday) select x).Count();
            FooterLabel.Text = count_in.ToString();
        
        }

        public async void DeleteButton_OnClicked(object sender, EventArgs e)
        {

             await (DisplayAlert("Notification", "Deleted all records", "Cancel"));
        }

        public async void DisplayButton_OnClicked(object sender, EventArgs e)
        {

            var datetoday = DateTime.Now.ToString("ddMMyyyy");
            List<SoccerAvailability> myList = (from x in conn.Table<SoccerAvailability>().Where(x => x.CurrentDate == datetoday) select x).ToList();
            if (myList != null)
            {
                SoccerAvailability soccerAvailability = new SoccerAvailability();
                soccerAvailability.SoccerStatus = myList[0].SoccerStatus;
                soccerAvailability.CurrentDate = DateTime.Now.ToString("ddMMyyyy");
                await Navigation.PushAsync(new SoccerDailyStatus(soccerAvailability) { });
            }
            else
            {
                await DisplayAlert("Notification", "Unable to proccess status", "Cancel");
            }
            
        }
  
    }
}