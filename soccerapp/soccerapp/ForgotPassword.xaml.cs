﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace soccerapp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ForgotPassword : ContentPage
    {
        private SQLiteConnection conn;
        string emailText;

        public ForgotPassword()
        {
            InitializeComponent();
            conn = DependencyService.Get<Isqlite>().GetConnection();
        }

        public async void btnSend_Clicked(object sender, EventArgs e)
        {
            emailText = txtEmail.Text;

            int count = (from x in conn.Table<PlayerDetails>().Where(x => x.Email == emailText) select x).Count();

            if (count != 0)
            {
                if(!string.IsNullOrWhiteSpace(txtEmail.Text))
                {
                    var detail = conn.Table<PlayerDetails>().First(x => x.Email == emailText);
                    var playerPassword = detail.Password;
                    try
                    {

                        MailMessage mail = new MailMessage();
                        SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                        mail.From = new MailAddress("admin@somedomain.com");
                        mail.To.Add(emailText);
                        mail.Subject = "Hello";
                        mail.Body = "Your soccer password: " + playerPassword;

                        SmtpServer.Port = 587;
                        SmtpServer.Host = "smtp.gmail.com";
                        SmtpServer.EnableSsl = true;
                        SmtpServer.UseDefaultCredentials = false;
                        SmtpServer.Credentials = new System.Net.NetworkCredential("admin@somedomain.com", "**************");
                        SmtpServer.Send(mail);
                        await DisplayAlert("Success", "Password send successfully!!", "Cancel");
                    }
                    catch (Exception ex)
                    {
                        await DisplayAlert("Failed", ex.Message, "OK");
                    }

                }
                else
                {
                    await DisplayAlert("Notification", "Forgot password field cannot be blank", "Cancel");
                }
               
            }
        }
    }
}