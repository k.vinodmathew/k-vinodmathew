﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace soccerapp
{
    public partial class MainPage : ContentPage, INotifyPropertyChanged
    {
        public SQLiteConnection conn = null;
        public PlayerDetails playermodel;
        ObservableCollection<PlayerDetails> players;
        public static bool IsUserLoggedIn { get; set; }


        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            var emailText = emailEntry.Text;
            var passwordText = passwordEntry.Text;            
            conn = DependencyService.Get<Isqlite>().GetConnection();
            conn.CreateTable<PlayerDetails>();
            players = new ObservableCollection<PlayerDetails>();
            this.BindingContext = this;
            this.IsBusy = false;
            
        }

       public async void Button_Clicked(object sender, System.EventArgs e)
        {
            this.IsBusy = true;
            await Task.Delay(TimeSpan.FromMilliseconds(1000));
            string emailText = emailEntry.Text;
            string passwordText= passwordEntry.Text; 
            if(!string.IsNullOrWhiteSpace(emailEntry.Text) && !string.IsNullOrWhiteSpace(passwordEntry.Text))
            {
               
                if(ValidateEmail(emailText) == true)
                {
                    
                    int count = (from x in conn.Table<PlayerDetails>().Where(x => x.Email == emailText) select x).Count();
                    if (count!= 0)
                    {
                        try
                        {
                           List<PlayerDetails> myList = (from x in conn.Table<PlayerDetails>().Where(x => x.Email == emailText && x.Password == passwordText) select x).ToList();
                            if(myList.Count() > 0)
                            {
                                    var tabbedPage = new TabbedPage();
                                    PlayerDetails playerDetails = new PlayerDetails();
                                    SoccerAvailability soccerAvailability = new SoccerAvailability();
                                    UpdateDetails updateDetails = new UpdateDetails();
                                    tabbedPage.Children.Add(new Home(emailEntry.Text));
                                    tabbedPage.Children.Add(new UpdatesNews(updateDetails, emailText, true));
                                    tabbedPage.Children.Add(new Map());
                                    tabbedPage.Children.Add(new Settings(soccerAvailability, emailEntry.Text, true));
                                   // tabbedPage.Children.Add(new Register(playerDetails));
                                    tabbedPage.Children.Add(new SoccerDailyStatus(soccerAvailability));
                                    await Navigation.PushAsync(tabbedPage);
                                    var profilePicTollbarItem = new ToolbarItem()
                                    {
                                        Icon = "LF.PNG"
                                    };
                                    profilePicTollbarItem.Clicked += OnProfilePicClicked;
                                    tabbedPage.ToolbarItems.Add(profilePicTollbarItem);
                            }
                            else
                            {
                                this.IsBusy = false;
                                await DisplayAlert("Notification", "No such email or password", "Cancel");

                            }
                            
                        }
                        catch (NullReferenceException ex)
                        {
                          if(ex!=null)
                            Debug.WriteLine("Something is thrown ! " + e.ToString());
                        }
                        finally
                        {
                            IsBusy = false;
                        }
                    }
                    else
                    {
                         this.IsBusy = false;
                        await DisplayAlert("Notification", "Unable to find the user details", "Cancel");
                    } 
                }
                else
                {
                    this.IsBusy = false;
                    await DisplayAlert("Notification", "Email is not a valid one", "Cancel");
                }   
            }
            else
            {
                this.IsBusy = false;
                await DisplayAlert("Notification","Input details cannot be blank", "Cancel");
            }
        }

        public async void NavigateButton_OnClicked(object sender, EventArgs e)
        {
            var tabbedPage = new TabbedPage();
            SoccerAvailability soccerAvailability = new SoccerAvailability();
            UpdateDetails updateDetails = new UpdateDetails();
            tabbedPage.Children.Add(new Home("Welcome"+' '+emailEntry.Text+' '+",have a nice day!"));
            tabbedPage.Children.Add(new UpdatesNews(updateDetails, emailEntry.Text, false));
            tabbedPage.Children.Add(new Map());
            tabbedPage.Children.Add(new Settings(soccerAvailability, emailEntry.Text, false));
            tabbedPage.Children.Add(new SoccerDailyStatus(soccerAvailability));
            await Navigation.PushAsync(tabbedPage);
        }

       public async void Register_OnClicked(object sender, EventArgs e)
       {
            PlayerDetails playerDetails = new PlayerDetails();
            await Navigation.PushAsync(new Register(playerDetails));
        }

        Regex EmailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        public bool ValidateEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            return EmailRegex.IsMatch(email);
        }
        private async void OnProfilePicClicked(object sender, EventArgs e)
        {
            //Navigate to Player Details screen here:
            var emailText = emailEntry.Text;            
            List<PlayerDetails> details = (from x in conn.Table<PlayerDetails>() where x.Email == emailText select x).ToList();
            if (details!= null)
            {
                // found the record:-
                PlayerDetails playerDetails = new PlayerDetails();
                playerDetails.FullName = details[0].FullName;
                playerDetails.Mobile = details[0].Mobile;
                playerDetails.SoccerPosition = details[0].SoccerPosition;
                playerDetails.PlayerImage = details[0].PlayerImage;
                playerDetails.Email = details[0].Email;
                await Navigation.PushAsync(new Register(playerDetails){ });
            }
            else
            {
              await DisplayAlert("Notification", "Can't find the playerDetails", "Cancel");
            }

        }

        public async void ForgotPassword_OnClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ForgotPassword() { });
        }


    }
}
