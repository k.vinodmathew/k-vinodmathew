﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace soccerapp
{
    public class SoccerAvailability: INotifyPropertyChanged
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public byte[] PlayerImage { get; set; }

        string fullname;
        string soccerstatus;
        string currentdate;
        string email;
        int count;

        public string FullName
        {
            set
            {
                if (fullname != value)
                {
                    fullname = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("FullName"));
                    }
                }
            }
            get
            {
                return fullname;
            }
        }

        public string Email
        {
            set
            {
                if (email != value)
                {
                    email = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Email"));
                    }
                }
            }
            get
            {
                return email;
            }
        }



        public string SoccerStatus
        {
            set
            {
                if (soccerstatus != value)
                {
                    soccerstatus = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("SoccerStatus"));
                    }
                }
            }
            get
            {
                return soccerstatus;
            }
        }

        public string CurrentDate
        {
            set
            {
                if (currentdate != value)
                {
                    currentdate = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("CurrentDate"));
                    }
                }
            }
            get
            {
                return currentdate;
            }
        }

        public int Count
        {
            set
            {
                if (count!= value)
                {
                    count = value;
                        if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Count"));
                    }
                }

            }
            get
            {
                return count;
            }
            

        }


        public event PropertyChangedEventHandler PropertyChanged;

       
    }
}
