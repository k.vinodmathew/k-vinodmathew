﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace soccerapp
{
    public interface Isqlite
    {
        SQLiteConnection GetConnection();
    }
}
