﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace soccerapp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Map : ContentPage
    {
        public Map()
        {
            InitializeComponent();
            backgroundImage.Source = new UriImageSource()
            {
            Uri = new Uri("https://picsum.photos/200/300"),
            CachingEnabled = false
            };
        }

      

    }
}