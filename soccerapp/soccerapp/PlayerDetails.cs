﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using Xamarin.Forms;

namespace soccerapp
{
    public class PlayerDetails : INotifyPropertyChanged
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public byte[] PlayerImage { get; set; }
        //public Image PlayerPic { get; set; }

        string fullname;
        string mobile;
        string soccerposition;
        string email;

        public PlayerDetails()
        {

        }

        [Ignore]
        public Image Image
        {
            get
            {
                var image = new Image();
                image.Source = ImageSource.FromStream(() => new MemoryStream(PlayerImage));
                return image;
            }
            set
            {
                
               //PlayerImage = Convert.ToByteArray(value.Source);
               //Bitmap.FromStream(inStream);
            }
        }


        public string FullName
        {
            set
            {
                if (fullname != value)
                {
                    fullname = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("FullName"));
                    }
                }
            }
            get
            {
                return fullname;
            }
        }

        public string Mobile
        {
            set
            {
                if (mobile != value)
                {
                    mobile = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Mobile"));
                    }
                }
            }
            get
            {
                return mobile;
            }

        }
        public string SoccerPosition
        {
            set
            {
                if (soccerposition != value)
                {
                    soccerposition = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("SoccerPosition"));
                    }
                }
            }
            get
            {
                return soccerposition;
            }
        }
        public string Email
        {
            set
            {
                if (email != value)
                {
                    email = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Email"));
                    }
                }
            }
            get
            {
                return email;
            }

        }

       //public ImageSource Source { get; internal set; }

       public event PropertyChangedEventHandler PropertyChanged;

    }


}
